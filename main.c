#include "Header.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <conio.h>
#include<string.h>

int main() {

	int choice;

	printf("   -----------------------------------------------------------------------------------------------------\n");
	printf("   |                                                                                                   |\n");
	printf("   |                                                                                                   |\n");
	printf("   |       O             O    O O O O    O          O O O O    O O O O    O O     O O    O O O O       |\n");
	printf("   |        O           O     O          O          O          O     O    O  O   O  O    O             |\n");
	printf("   |         O         O      O O O      O          O          O     O    O   O O   O    O O O         |\n");
	printf("   |          O  O O  O       O          O          O          O     O    O    O    O    O             |\n");
	printf("   |           O    O         O O O O    O O O O    O O O O    O O O O    O         O    O O O O       |\n");
	printf("   |                                                                                                   |\n");
	printf("   |                                             TO FERIT HOTEL                                        |\n");
	printf("   -----------------------------------------------------------------------------------------------------\n");

	printf("\n\n   Press any key to continue: ");

	_getch();
	system("cls");

	login();
	system("cls");



	while (1)
	{
		system("cls");

		printf("\n   ---------------  MAIN MENU  --------------- ");

		printf("\n   Please enter your choice for menu: ");
		printf("\n\n");
		printf("\n Enter 1 -> Book a room");
		printf("\n------------------------");
		printf(" \n Enter 2 -> View Customer Record");
		printf("\n----------------------------------");
		printf(" \n Enter 3 -> Delete Customer Record");
		printf("\n-----------------------------------");
		printf(" \n Enter 4 -> Search Customer Record");
		printf("\n-----------------------------------");
		printf(" \n Enter 5 -> Edit Record");
		printf("\n-----------------------");
		printf(" \n Enter 6 -> Exit");
		printf("\n-----------------\n");
		printf(" \n Input: ");

		scanf("%d", &choice);
		/*	char c;
			scanf("%c", &c);*/
			//fflush(stdin);
		getchar();
		switch (choice)
		{
		case 1: add(); break;

		case 2: list(); break;

		case 3: deleteCustomer(); break;

		case 4: search(); break;

		case 5: edit(); break;

		case 6:

			exit(0);
			break;

		default:
			system("cls");
			printf("\nIncorrect input\n\n");
			printf("\n Press any key to continue");
			_getch();
		}

	}

	return 0;

}