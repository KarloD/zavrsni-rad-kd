#ifndef HEADER_H
#define HEADER_H
#define _CRT_SECURE_NO_WARNINGS

struct customerDetails {

	char roomnumber[4];
	char name[30];
	char address[30];
	char phonenumber[15];
	char email[30];
	char daysStaying[5];
	char arrivalDate[15];
	char ID[10];

}CUSTOMERTDETAILS;

void add();
void list();
void edit();
void deleteCustomer();
void search();

#endif

